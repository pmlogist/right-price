# Le Juste Prix

Une simple jeu pour passer le temps.

## Dépendences
```
node >= 16.14.2
```

## Quickstart
```sh
git clone https://gitlab.com/pmlogist/right-price.git
npm install
npm run start
```

## Build
```sh
npm run make
```

## Binaries
[Télécharger les binaires Windows et Debian](https://drive.google.com/file/d/1nHdkwNKBnFkD52aGHd91t-LjvzTJQFiY/view?usp=sharing)

Le binaire Debian est build à travers un job dans la [pipeline]( https://gitlab.com/pmlogist/right-price/-/pipelines) et est disponible en artifcat.

Cependant, une phase de release serait plus appropriée en effectuant un push sur un release registry.