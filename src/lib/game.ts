export interface GameProps {
  attempts: number;
}

export type GameStatus = "start" | "more" | "less" | "loose" | "win";

export class Game {
  private baseAttempts: number;
  message: string;
  attempts: number;
  randomNb: number;
  status: GameStatus;

  constructor(props: GameProps) {
    const { attempts } = props;

    this.attempts = attempts;
    this.baseAttempts = this.attempts;
  }

  generateRandomNb() {
    return Math.floor(Math.random() * 50) + 1;
  }

  start() {
    this.randomNb = this.generateRandomNb();
    this.status = "start";
  }

  play(input: number): string {
    if (this.attempts === this.baseAttempts) {
      this.start();
    }
    this.decrement();

    if (input === this.randomNb) {
      this.status = "win";
      this.message = "Victoire !";
      return this.message;
    }

    if (input > this.randomNb) {
      this.status = "less";
      this.message = `C'est moins que ${input}`;
    } else if (input < this.randomNb) {
      this.status = "more";
      this.message = `C'est plus que ${input}`;
    }

    if (this.attempts === 0) {
      this.status = "loose";
      this.message = `Défaite... il fallait trouver ${this.randomNb}`;
      return this.message;
    }
  }

  decrement() {
    if (this.attempts > 0) {
      this.attempts = this.attempts - 1;
    }
  }

  reset() {
    this.attempts = this.baseAttempts;
    this.status = "start";
  }

  setAttemps(span: HTMLSpanElement) {
    span.textContent = this.attempts.toString();
  }
}
