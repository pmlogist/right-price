import "./styles/cleanup.css";
import "./styles/variables.css";
import "./styles/mixins.css";
import "./styles/index.css";
import { Game } from "./lib/game";

const attemptsSpan = <HTMLSpanElement>document.getElementById("attempts");
const guessInput = <HTMLInputElement>document.getElementById("guess");
const playButton = <HTMLButtonElement>document.getElementById("play-button");
const replayButton = <HTMLButtonElement>(
  document.getElementById("replay-button")
);
const notificationList = <HTMLUListElement>(
  document.getElementById("notification")
);
const gameStatus = <HTMLDivElement>document.getElementById("game-status");

const game = new Game({ attempts: 5 });
game.setAttemps(attemptsSpan);

playButton.addEventListener("click", async () => {
  replayButton.style.visibility = "visible";

  if (isNaN(parseInt(guessInput.value))) {
    alert("Heuu... il faut écrire un chiffre mon ami");
    return;
  }

  if (parseInt(guessInput.value) > 50) {
    alert("Faut pas exagérer non plus !");
    return;
  }

  if (guessInput.value.length) {
    game.play(parseInt(guessInput.value));
    gameStatus.textContent = game.message;
    game.setAttemps(attemptsSpan);

    if (game.status === "win") {
      playButton.disabled = false;
      gameStatus.style.visibility = "visible";
      gameStatus.className = "game-status victory";
      replayButton.style.visibility = "visible";
      return;
    }

    if (game.status === "loose") {
      game.decrement();
      playButton.disabled = true;
      gameStatus.style.visibility = "visible";
      gameStatus.textContent = game.message;
      gameStatus.className = "game-status defeat";
      return;
    }

    const li = document.createElement("li");
    li.appendChild(document.createTextNode(game.message));
    notificationList.appendChild(li);
  }
});

replayButton.addEventListener("click", async () => {
  game.reset();
  game.setAttemps(attemptsSpan);
  guessInput.value = (0).toString();
  playButton.disabled = false;
  gameStatus.textContent = "";
  gameStatus.className = "game-status";
  gameStatus.style.visibility = "hidden";
  notificationList.textContent = "";
  replayButton.style.visibility = "hidden";
});
