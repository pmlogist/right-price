import { Game } from "../src/lib/game";

describe("Game", () => {
  it("starts with random number to guess", () => {
    const game = new Game({ attempts: 5 });

    for (let i: number; i < 50; i++) {
      expect(game.randomNb >= 0 && game.randomNb <= 50);
    }
  });

  it.each([5, 4, 3, 2, 1])("finishes on a victory", (result: number) => {
    const game = new Game({ attempts: 5 });
    game.randomNb = 1;

    expect(game.status === "less");

    if (game.attempts === 0) {
      expect(game.randomNb === result);
      expect(game.status === "win");
    }
  });

  it.each([1, 2, 3, 4, 5])("finishes on a defeat", (result: number) => {
    const game = new Game({ attempts: 5 });
    game.randomNb = 6;

    expect(game.status === "more");

    if (game.attempts === 0) {
      expect(game.randomNb !== result);
      expect(game.status === "loose");
    }
  });
});
